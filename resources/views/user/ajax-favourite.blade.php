<form class="login100-form validate-form" method="post" action="/add/favourite">
    <div class="modal-header">
        <h5 class="modal-title text-success" id="exampleModalLabel">Do you want to add into favourite?</h5>
    </div>
    <div class="modal-body">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" readonly="" value="{{$getData->name}}">
                </div>
            </div>
        </div>
        <input type="hidden" name="getid" value="{{$getData->id}}" >
        <input type="hidden" name="name" value="{{$getData->name}}" >
    </div>
    
    <div class="modal-footer">
        <div class="left-side">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        <div class="divider"></div>
        @if($getData != null)
        <div class="right-side">
            <button type="submit" class="btn btn-info">Submit</button>
        </div>
        @endif
    </div>
</form>   