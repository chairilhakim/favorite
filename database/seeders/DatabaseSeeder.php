<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class DatabaseSeeder extends Seeder {
    
    public function run() {
        $company = array(
            array(
                'name' => 'Company', 
                'email' => 'main@company.com',
                'logo' => 'company.png',
                'website' => 'https://www.company.com/'
            ),
            array(
                'name' => 'Company2', 
                'email' => 'main@company2.com',
                'logo' => 'company.png',
                'website' => 'https://www.company2.com/'
            ),
            array(
                'name' => 'Company3', 
                'email' => 'main@company3.com',
                'logo' => 'company.png',
                'website' => 'https://www.company3.com/'
            ),
            array(
                'name' => 'Company4', 
                'email' => 'main@company4.com',
                'logo' => 'company.png',
                'website' => 'https://www.company4.com/'
            ),
            array(
                'name' => 'Company5', 
                'email' => 'main@company5.com',
                'logo' => 'company.png',
                'website' => 'https://www.company5.com/'
            ),
            array(
                'name' => 'Company6', 
                'email' => 'main@company6.com',
                'logo' => 'company.png',
                'website' => 'https://www.company6.com/'
            ),
            array(
                'name' => 'Company7', 
                'email' => 'main@company7.com',
                'logo' => 'company.png',
                'website' => 'https://www.company7.com/'
            ),
            array(
                'name' => 'Company8', 
                'email' => 'main@company8.com',
                'logo' => 'company.png',
                'website' => 'https://www.company8.com/'
            ),
            array(
                'name' => 'Company8', 
                'email' => 'main@company8.com',
                'logo' => 'company.png',
                'website' => 'https://www.company8.com/'
            ),
        );
        foreach($company as $rowCompany){
            DB::table('company')->insert($rowCompany);
        }
        
        $users = array(
            array(
                'name' => 'Admin',
                'phone' => '987678',
                'password' => bcrypt('password'),
                'email' => 'admin@company.com',
                'is_active' => 1,
                'user_type' => 1,
            ),
            array(
                'name' => 'User1',
                'phone' => '678654',
                'password' => bcrypt('password'),
                'email' => 'user1@company.com',
                'is_active' => 1,
                'user_type' => 10,
            ),
            array(
                'name' => 'User2',
                'phone' => '123452',
                'password' => bcrypt('password'),
                'email' => 'user2@company.com',
                'is_active' => 1,
                'user_type' => 10,
            ),
        );
        foreach($users as $row){
            DB::table('users')->insert($row);
        }
        
        dd('done seed');
    }
    
}
