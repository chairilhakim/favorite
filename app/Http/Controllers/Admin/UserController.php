<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

use App\Models\User;
use App\Models\Company;

class UserController extends Controller {

    public function __construct(){
        
    }
    
    public function getResetPassword(){
        $dataUser = Auth::user();
        $onlyUser  = array(10);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        return view('User.reset-password')
                ->with('dataUser', $dataUser);
    }

    public function postResetPassword(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(10);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        if($request->password == null || $request->repassword == null){
            return redirect()->route('resetPasswd')
                ->with('message', 'Data must not empty')
                ->with('messageclass', 'danger');
        }
        if($request->password != $request->repassword){
            return redirect()->route('resetPasswd')
                ->with('message', 'Validate password not identic')
                ->with('messageclass', 'danger');
        }
        $modelUser = New User;
        $dataUpdate = array(
            'password' => bcrypt($request->password),
            'updated_at' => date('Y-m-d H:i:s'),
        );
        $modelUser->getUpdateUsers('id', $dataUser->id, $dataUpdate);
        return redirect()->route('resetPasswd')
                ->with('message', 'password has updated')
                ->with('messageclass', 'success');
    }
    
    public function getUserCompany(){
        $dataUser = Auth::user();
        $onlyUser  = array(10);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        $modelCompany = New Company;
        $getAllCompany = $modelCompany->getAllCompanyforUser($dataUser->id);
        return view('user.company')
                ->with('getAllCompany', $getAllCompany)
                ->with('dataUser', $dataUser);
    }
    
    public function postAddFavouriteCompany(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(10);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        $modelCompany = New Company;
        $user_id = $dataUser->id;
        $company_id = $request->getid;
        $dataInsert = array(
            'user_id' => $user_id,
            'company_id' => $company_id,
        );
        $modelCompany->getInsertFavorite($dataInsert);
        return redirect()->route('userCompany')
                ->with('message', 'you have select company '.$request->name.' as favorite')
                ->with('messageclass', 'success');
    }
    
    public function postRemoveFavouriteCompany(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(10);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        $modelCompany = New Company;
        $user_id = $dataUser->id;
        $company_id = $request->getid;
        $dataUpdate = array(
            'user_id' => null,
            'deleted_at' => date('Y-m-d H:i:s'),
            'old_user_id' => $dataUser->id
        );
        $modelCompany->getUpdateFavorite('id', $request->fav_id, $dataUpdate);
        return redirect()->route('userCompany')
                ->with('message', 'you have select remove company '.$request->name.' from favorite')
                ->with('messageclass', 'success');
    }
    

}
