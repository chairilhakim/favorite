<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

use App\Models\User;
use App\Models\Company;

class MasteradminController extends Controller {

    public function __construct(){
        
    }
    
    public function getAllUser(){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        $modelUser = New User;
        $getAllUser = $modelUser->getAllUser();
        return view('admin.user.all-user')
                ->with('getAllUser', $getAllUser)
                ->with('dataUser', $dataUser);
    }
    
    public function getAddUser(){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        $modelUser = New User;
        $modelCompany = New Company;
        return view('admin.user.create-user')
                ->with('dataUser', $dataUser);
    }
    
    public function postAddUser(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        if($request->name == null || $request->email == null || $request->password == null){
            return redirect()->route('addUser')
                ->with('message', 'Data must not empty')
                ->with('messageclass', 'danger');
        }
        $modelUser = New User;
        $validateEmail = $modelUser->getEmailValidate($request->email);
        if($validateEmail != null){
           return redirect()->route('addUser')
               ->with('message', 'use another email')
               ->with('messageclass', 'danger');
        }
        $dataInsert = array(
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => bcrypt($request->password),
            'is_active' => 1,
        );
        $modelUser->getInsertUser($dataInsert);
        return redirect()->route('listUsers')
                ->with('message', 'Insert Successfully')
                ->with('messageclass', 'success');
    }
    
    public function getEditUser($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        $modelUser = New User;
        $modelCompany = New Company;
        $getData = $modelUser->getDetailUser($id);
        if($getData == null){
            return redirect()->route('listUsers')
                ->with('message', 'data not found')
                ->with('messageclass', 'danger');
        }
        return view('admin.user.edit-user')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function postEditUser(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        if($request->f_name == null || $request->l_name == null || $request->email == null){
            return redirect()->route('EditUser', [$request->getid])
                ->with('message', 'Data must not empty')
                ->with('messageclass', 'danger');
        }
        $modelUser = New User;
        $modelCompany = New Company;
        $validateEmail = $modelUser->getEmailValidateEdit($request->email, $request->getid);
        if($validateEmail != null){
            return redirect()->route('EditUser', [$request->getid])
                    ->with('message', 'use another email')
                    ->with('messageclass', 'danger');
        }
        $dataUpdate = array(
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
        );
        if($request->password != null){
            $dataUpdate = array(
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'password' => bcrypt($request->password),
            );
        }
        $modelUser->getUpdateUsers('id', $request->getid, $dataUpdate);
        return redirect()->route('listUsers')
                ->with('message', 'Update Successfully')
                ->with('messageclass', 'success');
    }
    
    public function postRemoveUser(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        $dataUpdate = array(
            'is_active' => 0,
            'deleted_at' => date('Y-m-d H:i:s')
        );
        $modelUser = New User;
        $modelCompany = New Company;
        $modelUser->getUpdateUsers('id', $request->getid, $dataUpdate);
        return redirect()->route('listUsers')
                ->with('message', 'Remove successfully')
                ->with('messageclass', 'success');
    }
    
    
    //Company
    public function getAllCompany(){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        $modelUser = New User;
        $modelCompany = New Company;
        $getAllCompany = $modelCompany->getAllCompany();
        return view('admin.company.all-company')
                ->with('getAllCompany', $getAllCompany)
                ->with('dataUser', $dataUser);
    }
    
    public function getAddCompany(){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        $modelUser = New User;
        $modelCompany = New Company;
        $getAllCompany = $modelCompany->getAllCompany();
        return view('admin.company.create-company')
                ->with('getAllCompany', $getAllCompany)
                ->with('dataUser', $dataUser);
    }
    
    public function postAddCompany(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        if($request->name == null){
            return redirect()->route('addCompany')
                ->with('message', 'name of Company not empty')
                ->with('messageclass', 'danger');
        }
        $modelUser = New User;
        $modelCompany = New Company;
        $validateCompany = $modelCompany->getCompanyValidate($request->name);
        if($validateCompany != null){
           return redirect()->route('website')
               ->with('message', 'use another company name')
               ->with('messageclass', 'danger');
        }
        $logo = null;
        if ($request->hasFile("image")) {
            $filenameWithExt = $request->file('image')->getClientOriginalName();
            $extension = $request->file('image')->getClientOriginalExtension();
            $logo = date('Ymd').'_'.time().'.'.$extension;
            $request->file('image')->storeAs('public', $logo);
        }
            
        $dataInsert = array(
            'name' => $request->name,
            'email' => $request->email,
            'logo' => $logo,
            'website' => $request->website        
        );
        $modelCompany->getInsertCompany($dataInsert);
        return redirect()->route('listCompany')
                ->with('message', 'Insert Successfully')
                ->with('messageclass', 'success');
    }
    
    public function getEditCompany($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        $modelUser = New User;
        $modelCompany = New Company;
        $getData = $modelCompany->getDetailCompany($id);
        if($getData == null){
            return redirect()->route('listCompany')
                ->with('message', 'data not found')
                ->with('messageclass', 'danger');
        }
        return view('admin.company.edit-company')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function postEditCompany(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        if($request->name == null){
            return redirect()->route('EditCompany', [$request->getid])
                ->with('message', 'Data must not empty')
                ->with('messageclass', 'danger');
        }
        $modelUser = New User;
        $modelCompany = New Company;
        $validateName = $modelCompany->getNameValidateEdit($request->name, $request->getid);
        if($validateName != null){
            return redirect()->route('EditCompany', [$request->getid])
                    ->with('message', 'use another company name')
                    ->with('messageclass', 'danger');
        }
        $logo = null;
        if ($request->hasFile("image")) {
            $filenameWithExt = $request->file('image')->getClientOriginalName();
            $extension = $request->file('image')->getClientOriginalExtension();
            $logo = date('Ymd').'_'.time().'.'.$extension;
            $request->file('image')->storeAs('public', $logo);
        }
            
        $dataUpdate = array(
            'name' => $request->name,
            'email' => $request->email,
            'logo' => $logo,
            'website' => $request->website        
        );
        $modelCompany->getUpdateCompany('id', $request->getid, $dataUpdate);
        return redirect()->route('listCompany')
                ->with('message', 'Update Successfully')
                ->with('messageclass', 'success');
    }
    
    public function postRemoveCompany(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        $dataUpdate = array(
            'is_active' => 0,
            'deleted_at' => date('Y-m-d H:i:s')
        );
        $modelUser = New User;
        $modelCompany = New Company;
        $modelUser->getUpdateUsers('id', $request->getid, $dataUpdate);
        return redirect()->route('listUsers')
                ->with('message', 'Remove successfully')
                ->with('messageclass', 'success');
    }
    
    
    
    
    

}
